import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Button from './components/Button';

class App extends Component {

  constructor(props){
      super(props);
      this.state = {
          clicks: 0
      }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <Button label={"Click me!"}/>
        </header>
      </div>
    );
  }
}

export default App;
