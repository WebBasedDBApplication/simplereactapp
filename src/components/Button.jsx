import React, { Component } from 'react';

class Button extends Component {

    constructor(props){
        super(props);
        this.state = {
            clicks: 0
        }
    }

    render() {
        return (
            <div>
                <button onClick={() => this.setState({clicks: this.state.clicks+1})}>{this.props.label}</button>
                <h1>You clicked {this.state.clicks} times!</h1>
            </div>
        );
    }
}

export default Button;
